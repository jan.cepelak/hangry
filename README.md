# Hangry App

This app serves one purpose and that is make lunch gatherings easier as we know live on three separate floors. You as a user can join or create new events for lunch (or dinner, whatever suits that time of a day).

## For developers

- ### Used technologies
  - Snowpack
  - TypeScript
  - React
  - Redux (for state management)
  - React-router-dom (for routing)
  - Jest (for testing)
  - Fastify
    - Firebase (as a database)